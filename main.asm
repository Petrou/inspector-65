.segment "HEADER"
.byte "NES" ;ines header
.byte $1a
.byte $02 ; PRG ROM 2*16KB
.byte $01 ; CHR ROM 1*8KB
.byte %00000000 ; mapping and mirroring
.byte $00
.byte $00
.byte $00
.byte $00
.byte $00, $00, $00, $00, $00 ;filler

.segment "ZEROPAGE"
;Controller refrences----------------------
aButton: .res 1
bButton: .res 1
selectButton: .res 1
startButton: .res 1
upButton: .res 1
downButton: .res 1
leftButton: .res 1
rightButton: .res 1


.segment "STARTUP"
;Player coordinates------------------------
.define charX $0203
.define charY $0200

;Controller pointer------------------------
.define joypad1 $4016

Reset:
	SEI; Disable all interrupts
	CLD; Disable decimal mode (not supported by NES6502)
	
	LDX #$40 
	STX $4017 ; Disable sound controller
	
	;Init stack
	LDX #$FF
	TXS
	
	INX ;X=0
	
	;Turn off drawing
	STX $2000
	STX $2001
	
	STX $4010 ;Disables last sound addres
	
: ;Wait for vBlank
	BIT $2002
	BPL :-
	
	TXA

clearMem: ;Sets the value of all ram to 0x00 except for 0200-02FF. That memory is set to be the PPU memory filled with 0xFF values.
	STA $0100, x ; Range[0100,01FF] = 0x00
	STA $0300, x  ; Range[0300,03FF] = 0x00
	STA $0400, x ; Range[0400,04FF] = 0x00
	STA $0500, x ; Range[0500,05FF] = 0x00
	STA $0600, x ; Range[0600,06FF] = 0x00
	STA $0700, x ; Range[0700,0800] = 0x00
	LDA #$FF ;Value to load in static VRAM
	STA $0200, x  ; Range[0200,02FF] = 0xFF
	LDA #$00 ; Value to load in CPU RAM
	INX ;Iterate
	BNE clearMem ;If stack overflowed memory is clear
	
: ;Wait for vBlank
	BIT $2002
	BPL :-
	
	LDA #$02 ;The high byte of the memory block that contains the PPU ram
	STA $4014 ;Loads the high byte into PPU memory
	NOP ;Blocks operations till end of CPU cycle
	
	;Sets the PPU mem adress to 3F00;
	LDA #$3F ;Higher bit
	STA $2006 
	LDA #$00 ;Lower bit
	STA $2006;
	
	LDX #$00
	
loadPalettes:
	LDA paletteData, x ;loads the palleteData into A
	STA $2007 ;Writes the pallet data into PPU 3F00(start) while iterating it to 3F1F (start+%32)
	INX
	CPX #$20 ;If x == 32 
	BNE loadPalettes ;Continue loading 
	LDX #$00
	
loadSprites:
	LDA spriteData, x ;loads sprite data into A
	STA $0200, x ;Writes the sprite data into RAM 0200 to 021F
	INX
	CPX #$10 ;If x == 32 
	BNE loadSprites ;Continue loading 
	
;Enable Interupts
	CLI
	
	LDA #%10010000 ;; Enable NMI and change background to second chr set
	STA $2000
	LDA #%00011110 ;Enabling sprites and background for left most 8 pixels
	;Enables sprites and rendering
	STA $2001
	
Loop:
	JMP Loop ;inf loop

	
NMI: ;;FRAME
	JSR pollInputs ;Checks for player input
	JSR controlPlayer ;Moves the player according to input
	
	LDA #$02 ;Copy the data from 0200(static VRAM) to PPU mem(Dynamic VRAM)
	STA $4014 ;The pointer address
	
	RTI; Interupt return
	
.include "joypad.inc"	 ;Includes the controller polling mechanism
.include "playerController.inc"	 ;Includes the player controller that is responsible for player movement

paletteData:
	.byte $22,$29,$1A,$0F,$22,$36,$17,$0f,$22,$30,$21,$0f,$22,$27,$17,$0F  ;background palette data
	.byte $22,$0F,$37,$27,$22,$1A,$30,$27,$22,$16,$30,$27,$22,$0F,$36,$17  ;sprite palette data

spriteData:
	;   0200    1    2    3
	;		y	sp	 pal  x
	.byte $08, $00, $00, $08 ;0207
	.byte $08, $00, %01000000, $10 ;020B
	.byte $10, $01, %01000000, $08 ;020F
	.byte $10, $01, $00, $10 ;0214
		
.segment "VECTORS"
	.word NMI ;Defines the NMI interrupt
	.word Reset ;Defines the Reset interrupt
	; spec hardware interrupt

	
.segment "CHARS"
	.incbin "sprites.chr" ;Loads the chr file
