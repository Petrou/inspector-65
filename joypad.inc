pollInputs:
	LDA #$01
	STA joypad1 ;Loads 01 into joypad poll making it init a poll
	LSR a ;Makes a = 0
	STA joypad1 ;Loads 00 into joypad making it finish the poll
	LDX #$00
pollLoop:
	LDA joypad1 ;Loads the button into A
	AND #1 ;Checks if pressed
	BNE pressed ;If pressed == true
	LDA #$00
	STA aButton, x ;Sets the according memory spot to false
	INX ;Increases the iterator
	CPX #$08 ;If x == 8
	BEQ endPoll ; If al buttons are polled return to main loop
	JMP pollLoop ; Polling not ready, poll next button

pressed:
	LDA #$01
	STA aButton, x ;Sets the according memory spot to true
	INX ;Increases the iterator
	CPX #$08 ;If x == 8 Checks if all buttons are polled
	BEQ	endPoll ; If al buttons are polled return to main loop
	JMP pollLoop ; Polling not ready, poll next button
	
endPoll: 
	RTS ; Return to main loop