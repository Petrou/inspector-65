controlPlayer:
	LDX rightButton
	CPX #$01
	BEQ moveRight
endRight:
	LDX leftButton
	CPX #$01
	BEQ moveLeft
endLeft: 
	LDX upButton
	CPX #$01
	BEQ moveUp
endUp: 
	LDX downButton
	CPX #$01
	BEQ moveDown
endDown:
controlPlayerRet:
	RTS

moveRight:
	LDX #$00
startMoveRight:
	LDY charX, x
	INY
	TYA
	STA charX, x
	INX
	INX
	INX
	INX
	CPX #$10
	BEQ endRight
	JMP startMoveRight
	
moveLeft:
	LDX #$00
startMoveLeft:
	LDY charX, x
	DEY
	TYA
	STA charX, x
	INX
	INX
	INX
	INX
	CPX #$10
	BEQ endLeft
	JMP startMoveLeft

moveUp:
	LDX #$00
startMoveUp:
	LDY charY, x
	DEY
	TYA
	STA charY, x
	INX
	INX
	INX
	INX
	CPX #$10
	BEQ endUp
	JMP startMoveUp
	
moveDown:
	LDX #$00
startMoveDown:
	LDY charY, x
	INY
	TYA
	STA charY, x
	INX
	INX
	INX
	INX
	CPX #$10
	BEQ endDown
	JMP startMoveDown
	